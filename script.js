/*jshint esversion: 6, moz: true */

let staffDeadline = new Date("Jul 20, 2020 00:00:00").getTime();
let studentDeadline = new Date("Aug 5, 2020 00:00:00").getTime();
var deadline = studentDeadline;
var x = setInterval(updateTime, 1000);
updateTime();


function tl(num) {
    return num.toLocaleString("fi-FI", {
        minimumIntegerDigits: 2,
        useGrouping: false
    });
}


function updateTime() {
    let now = new Date().getTime();
    let t = deadline - now;
    let days = Math.floor(t / (1000 * 60 * 60 * 24));
    let hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((t % (1000 * 60)) / 1000);
    document.getElementById("timer").innerHTML = `${tl(days)}:${tl(hours)}:${tl(minutes)}:${tl(seconds)}`;
    if (t < 0) {
        clearInterval(x);
        x = undefined;
        document.getElementById("timer").innerHTML = "00:00:00:00";
        document.getElementById("head").innerHTML = "SISU ON TULLUT!";
    } else {
        if (x === undefined) {
            x = setInterval(updateTime, 1000);
            document.getElementById("head").innerHTML = "SISU TULEE - OLETKO VALMIS?";
        }
    }
}


document.getElementById("students-btn").addEventListener("click", function() {
    deadline = studentDeadline;
    this.classList.add("active-button");
    document.getElementById("staff-btn").classList.remove("active-button");
    updateTime();
});

document.getElementById("staff-btn").addEventListener("click", function () {
    deadline = staffDeadline;
    this.classList.add("active-button");
    document.getElementById("students-btn").classList.remove("active-button");
    updateTime();
});